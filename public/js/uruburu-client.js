require(['../bower_components/d3/d3', 
	'socket.io/socket.io.js',
	'../bower_components/uruburu/uruburu'], function(d3, io) {

	var data, 
		inBindings = {},
		outBindings = {},
		start = new Date().getTime(),
		host = window.location.host.split(':')[0];

	var socket = io.connect(host + '/mqtt');
  socket.on('mqtt', function (data) {
  	var message = data.message,
  		topic = data.topic;
		for (var e in inBindings) {
			if (message[inBindings[e].data]) {
				inBindings[e].obj.setValue(message[inBindings[e].data].value);
			}
		}
  });

	socket.emit('mqtt_subscribe', { topic: 'uruburu/data' });

	function bindPolymer(obj, attrName, dataPath, out) {
		if (!out) {
			inBindings[obj.id] = { obj: obj, attr: attrName, data: dataPath };
		} else {
			outBindings[obj.id] = { obj: obj, attr: attrName, data: dataPath };
		}
	}

	function findPolymerBindings() {
		var root = d3.select('body').selectAll('*').each(function() {
			var e = d3.select(this).call(function() {
				var attributes = this[0][0].attributes;
				for (var i = 0; i < attributes.length; i++) {
					if (attributes[i].value.match(/\@\{\{.*\}\}/)) {
						bindPolymer(this[0][0], 
							attributes[i].name, 
							attributes[i].value.replace('@{{', '').replace('}}', ''),
							'out');
					} else if (attributes[i].value.match(/\{\{.*\}\}/)) {
						bindPolymer(this[0][0], 
							attributes[i].name, 
							attributes[i].value.replace('{{', '').replace('}}', ''));
					}
				}
			});
		});
	}

	findPolymerBindings();

	setInterval(function() {
		var data;
		for (var o in outBindings) {
			var current = outBindings[o].obj.getValue();
			if (outBindings[o].value != current) {
				if (!data) data = {};
				data[outBindings[o].data] = { value: current };
				outBindings[o].value = current;
			}
		}

		if (data) {
			socket.emit('mqtt', {
				topic: 'uruburu/data', 
				message: JSON.stringify(data)
			});
		}
	}, 100);

	uruburu.registerCallback('cbTest', function() {
		console.log('callback called');
	})
});