#!/usr/local/bin/node

var myIP = require('my-ip'),
  mqtt = require('mqtt');

var client = mqtt.createClient(1883, myIP());
var t0 = new Date().getTime();

setInterval(function() {
	t = new Date().getTime();
  client.publish('uruburu/data', 
  	JSON.stringify({
  		'x': { value: Math.sin((t-t0)/10000*2*Math.PI) * 50 },
  		'y': { value: Math.sin((t-t0)/10000*2*Math.PI) * 50 }
  	})
  );
}, 100);