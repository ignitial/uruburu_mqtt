#!/usr/local/bin/node

var uruburu = require('../index');

var server = new uruburu.MQTTBridge({
  port: 13031,
  mqtt: {
  	port: 1883,
  	ip: "192.168.0.13"
  },
  baseDir: '../',
  debug: true,
  jade: false
});
