var myIP = require('my-ip'),
	express = require('express'),
  bodyParser = require('body-parser'),
  http = require('http'),
  mqtt = require('mqtt');

exports.MQTTBridge = function(options) {
  var self = this;
  options = options || {};

  options.port = options.port || 13031;
  options.ip = options.ip || myIP();
  options.baseDir = options.baseDir || __dirname;
  options.viewsDir = options.viewsDir || (options.baseDir + '/views');
  options.publicDir = options.publicDir || (options.baseDir + '/public');
  options.mqtt = options.mqtt || {}
  options.mqtt.ip = options.mqtt.ip || 'localhost';
  options.mqtt.port = options.mqtt.port || 1883;

  function info() {
    if (options.debug) {
      var args = Array.prototype.slice.call(arguments, 0);
      console.log.apply(console, args);
    }
  }

  self.app = express();
  self.topics = {}
    
  var server = http.createServer(self.app);
  var io = require('socket.io')(server);
  var mqtt_io = io.of('/mqtt');

  mqtt_io.on('connection', function (socket) {
    var mqttBridge = mqtt.createClient(options.mqtt.port, options.mqtt.ip);
    mqttBridge.on('message', function(topic, message) {
      message = JSON.parse(message);
      for (var v in message) {
        self.topics[topic][v] = message[v];
      }
      socket.emit('mqtt', { topic: topic, message: message });
      if (options.onPublished) {
        options.onPublished(topic, message);
      }
    });

    socket.on('mqtt', function (data) { 
      for (var v in data.message) {
        if (!self.topics[data.topic]) self.topics[data.topic] = {};
        self.topics[data.topic][v] = data.message[v];
      }
      mqttBridge.publish(data.topic, data.message);
      if (options.onPublished) {
        options.onPublished(data.topic, data.message);
      }
    });

    socket.on('mqtt_subscribe', function(data) {
      mqttBridge.subscribe(data.topic);
      socket.emit(data.topic, self.topics[data.topic]);

      if (options.onSuscribed) {
        options.onSubscribed(data.topic, client);
      }
    });

    socket.on('mqtt_unsubscribe', function(data) {
      mqttBridge.unsubscribe(data.topic);
    });

    socket.on('disconnect', function() {
      mqttBridge.end();
    });
  });

  self.app.set('views', options.viewsDir);
  if (options.jade) {
    self.app.set('view engine', 'jade');
  }
  self.app.use(require('serve-favicon')(options.publicDir + '/favicon.png'));
  self.app.use(bodyParser.json());
  self.app.use(bodyParser.urlencoded({
    extended: true
  }));

  self.app.use('/', express.static(options.publicDir));

  if (options.jade) {
    self.app.get('/', function(req, res, next) {
      res.render('index');
    });
  }

  server.listen(options.port, options.ip, function () { 
    info('Web server running at ' + options.ip + ':' + options.port);
    if (options.onReady) options.onReady();
  });
}
